FROM nginx:stable-alpine

# Version
LABEL version=1.0
# set the maintainer
LABEL maintainer "cesar.soto@dtpnetwork.biz"

# Timezone
ENV TZ America/Mexico_City

COPY . /usr/share/nginx/html
RUN rm /usr/share/nginx/html/Dockerfile

EXPOSE 80/tcp

CMD ["nginx", "-g", "daemon off;"]
